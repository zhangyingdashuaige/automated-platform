package com.capgemini.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.entity.ModuleEntity;
import com.capgemini.mapper.ModuleMapper;

@Service
public class ModuleService {
	
	@Autowired
	ModuleMapper moduleMapper;

	public void insert(ModuleEntity moduleEntity) {
		
		moduleMapper.insert(moduleEntity);
	}

	public ModuleEntity selectById(Integer moduleId) {
		
		return moduleMapper.selectById(moduleId);
	}
	

	public void deleteById(Integer moduleId) {
		moduleMapper.deleteById(moduleId);
	}

	
	public void update(ModuleEntity moduleEntity) {
		moduleMapper.update(moduleEntity);
	}

	public List<ModuleEntity> selectAll(Integer projectId) {
		
		return moduleMapper.selectAll(projectId);
	}

}
