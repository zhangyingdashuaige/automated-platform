package com.capgemini.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.entity.TUserEntity;
import com.capgemini.mapper.TUserMapper;

@Service
public class TUserService {
	
	@Autowired
	TUserMapper tUserMapper;
	
	public List<TUserEntity> selectAll(){
		return tUserMapper.selectAll();
	}
	
	public List<TUserEntity> selectById(Integer userId){
		return tUserMapper.selectById(userId);
	}
	
	
}
