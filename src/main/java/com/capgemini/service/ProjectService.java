package com.capgemini.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.capgemini.entity.ProjectEntity;
import com.capgemini.mapper.ProjectMapper;


@Service
public class ProjectService {

	@Autowired
	ProjectMapper projectMapper;
	
	/**
	 * 增加project
	 * @param projectEntity
	 */
	public void insert(ProjectEntity projectEntity){
		projectMapper.insert(projectEntity);
	}
	
	/**
	 * 根据ID查找project
	 * @param projectId
	 * @return
	 */
	public ProjectEntity selectById( Integer projectId){
		return projectMapper.selectById(projectId);
	}
	

	/**
	 * 查找所有的projectt
	 * @return
	 */
	public List<ProjectEntity> selectAll(){
		return projectMapper.selectAll();
	}
	
	public void deleteById(Integer projectId){
		projectMapper.deleteById(projectId);
	}

	public void update(ProjectEntity projectEntity) {
		projectMapper.update(projectEntity);
	}
	
}


