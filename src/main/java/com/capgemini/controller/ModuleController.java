package com.capgemini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.entity.ModuleEntity;
import com.capgemini.service.ModuleService;



//@RestController是@Controller和@ResponseBody的合体
@RestController
@RequestMapping(value = "/modulePage")
public class ModuleController {

	@Autowired
	ModuleService moduleService;
  
	
	//@GetMapping("/search")	//查询时用get
	
	/**
	 * 增加模块
	 * @param moduleEntity
	 */
	@PostMapping("/insert")
							//@RequestBody		//接收实体类
  public void insert(@RequestBody ModuleEntity moduleEntity){
		moduleService.insert(moduleEntity);
  }
	
	/**
	 * 根据Id查找模块
	 * @param moduleId
	 * @return
	 */
	@GetMapping("/querybyid")
	public ModuleEntity selectById(@RequestParam Integer moduleId){
		return moduleService.selectById(moduleId);
	}
	
	
	/**
	 * 查找某个项目下所有的模块
	 * @return
	 */
	@GetMapping("/queryAll")
	public List<ModuleEntity> selectAll(Integer projectId){
		
		return moduleService.selectAll(projectId);
	}
	
	
	/**
	 * 根据Id删除模块
	 * @param projectId
	 */
	@PostMapping("/delete")
	public void deleteById(@RequestParam Integer moduleId){
		moduleService.deleteById(moduleId);
	}
	
	/**
	 * 修改模块
	 * @param projectEntity
	 */
	@PostMapping("/update")
	public void update(@RequestBody ModuleEntity moduleEntity){
		
		moduleService.update(moduleEntity);
	}
	
}

