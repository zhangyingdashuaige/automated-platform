package com.capgemini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.entity.ProjectEntity;
import com.capgemini.service.ProjectService;


//@RestController是@Controller和@ResponseBody的合体
@RestController
@RequestMapping(value = "/projectPage")
public class ProjectController {

	@Autowired
	ProjectService projectService;
    
	
	//@GetMapping("/search")	//查询时用get
	
	/**
	 * 增加项目
	 * @param projectEntity
	 */
	@PostMapping("/insert")
							//@RequestBody		//接收实体类
    public void insert(@RequestBody ProjectEntity projectEntity){
		//projectEntity.setProjectName("罗氏");
		projectService.insert(projectEntity);
    }
	
	/**
	 * 根据Id查找项目
	 * @param projectId
	 * @return
	 */
	@GetMapping("/querybyid")
	public ProjectEntity selectById(@RequestParam Integer projectId){
		return projectService.selectById(projectId);
	}
	
	
	/**
	 * 查找所有的项目
	 * @return
	 */
	@GetMapping("/queryAll")
	public List<ProjectEntity> selectAll(){
		return projectService.selectAll();
	}
	
	/**
	 * 根据Id删除项目
	 * @param projectId
	 */
	@PostMapping("/delete")
	public void deleteById(@RequestParam Integer projectId){
		projectService.deleteById(projectId);
	}
	
	/**
	 * 修改项目
	 * @param projectEntity
	 */
	@PostMapping("/update")
	public void update(@RequestBody ProjectEntity projectEntity){
		
		projectService.update(projectEntity);
	}
	
}
