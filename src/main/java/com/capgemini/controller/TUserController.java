package com.capgemini.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.entity.TUserEntity;
import com.capgemini.service.TUserService;

//@RestController是@Controller和@ResponseBody的合体
@RestController
@RequestMapping(value = "/userPage")
public class TUserController{

	@Autowired
	TUserService tUserService;
    
	@GetMapping("/queryall")
    public List<TUserEntity> selectAll(){
        return tUserService.selectAll();
    }
	
	@GetMapping("/querybyid")
    public List<TUserEntity> selectById(@RequestParam Integer userId){
        return tUserService.selectById(userId);
    }
	
}
