package com.capgemini.mapper;

import java.util.List;

import com.capgemini.entity.ProjectEntity;

public interface ProjectMapper {

	void insert(ProjectEntity projectEntity);
	
	ProjectEntity selectById(Integer projectId);
	
	List<ProjectEntity> selectAll();
	
	void deleteById(Integer projectId);

	void update(ProjectEntity projectEntity);
}
