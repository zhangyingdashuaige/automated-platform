package com.capgemini.mapper;


import java.util.List;

import com.capgemini.entity.ModuleEntity;

public interface ModuleMapper {

	void insert(ModuleEntity moduleEntity);

	ModuleEntity selectById(Integer moduleId);

	void deleteById(Integer moduleId);

	void update(ModuleEntity moduleEntity);

	List<ModuleEntity> selectAll(Integer projectId);

	
	
}
