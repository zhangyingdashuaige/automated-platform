package com.capgemini.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.capgemini.entity.TUserEntity;
/**
 * 注解版的mybatis,括号里直接加sql语句，适合不复杂的查询，详情百度
 */
public interface TUserMapper {

	//@Param就是指定接收的参数名，不是这个名就不接
	@Update("UPDATE user SET username=#{userName},password=#{passWord},phone=#{phone} WHERE user_id=#{userId}")
	int updateById(@Param("userId") Integer userId, @Param("userName") String userNameeeee, @Param("passWord") String passWord, @Param("phone") String phone);

	@Select("select * from user")
	@Results({ @Result(property = "userId", column = "user_id"),
			@Result(property = "userName", column = "username") ,
			@Result(property = "passWord", column = "password"),
			@Result(property = "phone", column = "phone")})
	List<TUserEntity> selectAll();
	
	List<TUserEntity> selectById(Integer userId);
}
