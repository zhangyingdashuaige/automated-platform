package com.capgemini.entity;

public class CaseEntity {
	
	private Integer caseId;
	private String caseName;
	private String moduleId;
	public CaseEntity() {
		super();
		
	}
	public CaseEntity(Integer caseId, String caseName, String moduleId) {
		super();
		this.caseId = caseId;
		this.caseName = caseName;
		this.moduleId = moduleId;
	}
	public Integer getCaseId() {
		return caseId;
	}
	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}
	public String getCaseName() {
		return caseName;
	}
	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	

}
