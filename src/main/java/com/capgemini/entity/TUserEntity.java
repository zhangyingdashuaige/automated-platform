package com.capgemini.entity;

public class TUserEntity {
    private Integer userId;

    private String userName;

    private String passWord;

    private String phone;
    
    public TUserEntity() {
		super();
	}

    public TUserEntity(Integer userId, String userName, String passWord, String phone) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.passWord = passWord;
		this.phone = phone;
	}
    
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
