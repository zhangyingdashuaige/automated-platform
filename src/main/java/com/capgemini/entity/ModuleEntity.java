package com.capgemini.entity;

public class ModuleEntity {

	private Integer moduleId;
	private String moduleName;
	private Integer 	projectId;
	public ModuleEntity() {
		super();
		
	}
	public ModuleEntity(Integer moduleId, String moduleName, Integer projectId) {
		super();
		this.moduleId = moduleId;
		this.moduleName = moduleName;
		this.projectId = projectId;
	}
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	
}
