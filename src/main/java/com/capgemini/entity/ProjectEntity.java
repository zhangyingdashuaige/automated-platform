package com.capgemini.entity;

public class ProjectEntity {

	private Integer projectId;
	
	private String projectName;
	
	public ProjectEntity(){
		super();
	}
	
	
	public ProjectEntity(Integer projectId,String projectName){
		super();
		this.projectId=projectId;
		this.projectName=projectName;
	}

	
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	
}
