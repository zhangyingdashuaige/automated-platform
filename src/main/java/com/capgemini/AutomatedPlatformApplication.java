package com.capgemini;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.capgemini.mapper")
public class AutomatedPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomatedPlatformApplication.class, args);
	}

}
